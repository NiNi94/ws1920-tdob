from setuptools import setup

setup(
    name='ws1920-tdob',
    version='0.0.1',
    packages=['tdob', 'tdob.test'],
    url='https://bitbucket.org/NiNi94/ws1920-twob/src/master/',
    license='apache',
    author='luca giulianini',
    author_email='luca.giulianini2@studio.unibo.it',
    description='Telegram bot for wather broadcasting using ontologies'
)
