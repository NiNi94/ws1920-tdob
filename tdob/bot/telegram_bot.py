import logging
import os

from telegram.ext import CommandHandler, ConversationHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram.ext import Updater

from bot.conversation import root, menu
from bot.conversation.fsm import bot_states, bot_events
from bot.utils import bot_utils
from ontology.owl_manager import OwlManager
from services.home_update_service import HomeUpdateService
from services.weather_update_service import WeatherUpdateService
from utils import utils

logger = logging.getLogger(os.path.basename(__file__))


class TelegramBot:
    def __init__(self, config, ontology_manager: OwlManager, weather_service: WeatherUpdateService,
                 home_service: HomeUpdateService):
        # Constructor
        self.ontology_manager = ontology_manager
        self.weather_service: WeatherUpdateService = weather_service
        self.home_service: HomeUpdateService = home_service
        self.config = config
        self.updater = Updater(token=config["telegram"]["token"], use_context=True)
        self.bot = self.updater.bot
        self.dispatcher = self.updater.dispatcher

        # Commands
        self.utils = bot_utils.BotUtils(config, self.bot)
        self.root = root.RootCommand(config, self.utils, self.ontology_manager, self.weather_service, home_service)
        self.menu = menu.MenuCommand(config, self.utils, self.ontology_manager)

        # FSM
        # Level 1 only callback
        self.menu_handler = ConversationHandler(
            entry_points=[
                CallbackQueryHandler(self.menu.show_weather_report_keyboard,
                                     pattern='^' + str(bot_events.W_REPORT_CLICK) + '$'),
                CallbackQueryHandler(self.menu.show_home_report_keyboard,
                                     pattern='^' + str(bot_events.H_REPORT_CLICK) + '$'),
                CallbackQueryHandler(self.root.logout, pattern='^' + str(bot_events.LOGOUT_CLICK) + '$'),
            ],
            states={
                bot_states.REPORT: [
                    CallbackQueryHandler(self.menu.report_resp,
                                         pattern="^(?!" + str(bot_events.BACK_W_CLICK) + "|" + str(
                                             bot_events.EXIT_CLICK) + "|" + bot_events.BACK_H_CLICK + ").*")]
            },
            fallbacks=[
                CallbackQueryHandler(self.menu.show_weather_report_keyboard,
                                     pattern='^' + str(bot_events.BACK_W_CLICK) + '$'),
                CallbackQueryHandler(self.menu.show_home_report_keyboard,
                                     pattern='^' + str(bot_events.BACK_H_CLICK) + '$'),
                CallbackQueryHandler(self.root.exit, pattern='^' + str(bot_events.EXIT_CLICK) + '$')
            ],
            per_message=True,
            map_to_parent={
                bot_states.END: bot_states.LOGGED,
                bot_states.LOGGED: bot_states.LOGGED,
                bot_states.NOT_LOGGED: bot_states.NOT_LOGGED
            }
        )
        # Level 0
        self.conversationHandler = ConversationHandler(
            entry_points=[CommandHandler('start', callback=self.root.start)],
            states={
                bot_states.NOT_LOGGED: [CommandHandler('login', callback=self.root.login)],
                bot_states.CREDENTIALS: [MessageHandler(filters=Filters.text, callback=self.root.credentials)],
                bot_states.LOGGED: [CommandHandler('menu', callback=self.root.show_logged_menu), self.menu_handler],
            },
            fallbacks=[CallbackQueryHandler(self.root.exit, pattern='^' + str(bot_events.EXIT_CLICK) + '$')]
        )
        # Init handlers
        self.dispatcher.add_handler(self.conversationHandler)

    def start_web_hook(self):
        # START WEBHOOK
        network = self.config["network"]["telegram"]
        key = utils.get_project_relative_path(network["key"])
        cert = utils.get_project_relative_path(network["cert"])
        utils.start_web_hook(self.updater, self.config["token"], network["ip"], network["port"], key, cert)
        logger.info("Started Webhook bot")

    def start_polling(self):
        logger.info("Started Polling bot")
        self.updater.start_polling()

    def get_bot(self):
        return self.bot
