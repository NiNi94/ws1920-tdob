import logging
import os

from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackContext

from bot.conversation.fsm import bot_events, bot_states
from bot.utils.bot_utils import BotUtils
from ontology.owl_manager import OwlManager

logger = logging.getLogger(os.path.basename(__file__))


class MenuCommand(object):
    # Constructor
    def __init__(self, config, conversation_utils: BotUtils, ontology_manager: OwlManager):
        self.config = config
        self.utils = conversation_utils
        self.ontology_manger: OwlManager = ontology_manager

    @staticmethod
    def show_weather_report_keyboard(update: Update, _):
        update.callback_query.answer()
        text = "Insert the day starting from now | ex: today = 0, yesterday = 1, ecc"
        kb = [[InlineKeyboardButton(str(n), callback_data="W:{}".format(n)) for n in range(0, 7)],
              [InlineKeyboardButton(text="❌", callback_data=str(bot_events.EXIT_CLICK))]]
        kb_markup = InlineKeyboardMarkup(kb)
        update.callback_query.edit_message_text(text=text, reply_markup=kb_markup)
        return bot_states.REPORT

    @staticmethod
    def show_home_report_keyboard(update: Update, _):
        update.callback_query.answer()
        text = "Insert the day starting from now | ex: today = 0, yesterday = 1, ecc"
        kb = [[InlineKeyboardButton(str(n), callback_data="H:{}".format(n)) for n in range(0, 7)],
              [InlineKeyboardButton(text="❌", callback_data=str(bot_events.EXIT_CLICK))]]
        kb_markup = InlineKeyboardMarkup(kb)
        update.callback_query.edit_message_text(text=text, reply_markup=kb_markup)
        return bot_states.REPORT

    def report_resp(self, update: Update, context: CallbackContext):
        resp = update.callback_query.data
        chat_id = update.effective_chat.id
        user = context.user_data[chat_id]["user_auth"]

        data = resp.split(":")
        command = data[0]
        value = data[1]
        back_event = bot_events.BACK_H_CLICK
        conditions = ""
        if command == 'W':
            back_event = bot_events.BACK_W_CLICK
            conditions = "Reports:\n{}".format(self.ontology_manger.get_weather_condition(user, int(value)))
        elif command == 'H':
            back_event = bot_events.BACK_H_CLICK
            conditions = "Reports:\n{}".format(self.ontology_manger.get_home_condition(user, int(value)))
        pass
        keyboard = [
            [InlineKeyboardButton(text="❌", callback_data=str(back_event))]]  # Exit if you want to exit
        reply_markup = InlineKeyboardMarkup(keyboard)

        update.callback_query.answer()
        update.callback_query.edit_message_text(text=conditions, reply_markup=reply_markup)
        return bot_states.REPORT
