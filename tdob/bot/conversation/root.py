import logging
import os

from telegram import ParseMode, InlineKeyboardButton, InlineKeyboardMarkup
from telegram import Update
from telegram.ext import ConversationHandler

from bot.conversation.fsm import bot_states, bot_events
from bot.utils.bot_utils import BotUtils
from ontology.owl_manager import OwlManager
from services.home_update_service import HomeUpdateService
from services.weather_update_service import WeatherUpdateService

logger = logging.getLogger(os.path.basename(__file__))


class RootCommand(object):
    # Constructor
    def __init__(self, config, conversation_utils: BotUtils, ontology_manager: OwlManager,
                 weather_service: WeatherUpdateService, home_service: HomeUpdateService):
        self.config = config
        self.utils = conversation_utils
        self.ontology_manger: OwlManager = ontology_manager
        self.weather_service: WeatherUpdateService = weather_service
        self.home_service: HomeUpdateService = home_service

    # STATE=START
    def start(self, update: Update, context):
        chat_id = update.effective_chat.id
        username = update.effective_user["username"]
        # Store value
        if chat_id not in context.user_data:
            self.utils.init_user(update, context, chat_id, username)
        if context.user_data[chat_id]["logged"]:
            update.callback_query.answer()
            update.effective_message.delete()
            return bot_states.LOGGED
        else:
            text = "Welcome to *Weather Ontology Bot* by *Luca Giulianini* [link](" \
                   "https://bitbucket.org/NiNi94/ws1920-tdob/)\n"
            message = context.bot.send_message(chat_id, text=text, parse_mode=ParseMode.MARKDOWN_V2)
            self.utils.check_last_and_delete(update, context, message)
            context.user_data[chat_id]["logged"] = False
            return bot_states.NOT_LOGGED

    def login(self, update, context):
        message = update.message.reply_text(text="Send me bot credentials: <username>:<password>", reply_markup=None)
        self.utils.check_last_and_delete(update, context, message)
        update.message.delete()
        return bot_states.CREDENTIALS

    def credentials(self, update, context):
        # User and chatid
        chat_id = update.effective_chat.id
        # Get config
        message = update.message.text
        splitted = message.split(':')
        username = splitted[0]
        password = splitted[1]
        update.message.delete()
        if self.ontology_manger.check_user_credentials(username, password):
            context.user_data[chat_id]["logged"] = True
            context.user_data[chat_id]["user_auth"] = username
            context.user_data[chat_id]["domotic_info"]["home_name"] = self.ontology_manger.get_home_name(username)
            context.user_data[chat_id]["domotic_info"]["city_name"] = self.ontology_manger.get_city_name(username)
            context.user_data[chat_id]["domotic_info"]["sensors"] = self.ontology_manger.get_sensors_name(username)
            message_sent = context.bot.send_message(chat_id, text="✅ Authentication succeded\n{}"
                                                    .format(self.utils.get_user_infos_message(context, chat_id,
                                                                                              username)))
            self.weather_service.city = context.user_data[chat_id]["domotic_info"]["city_name"]
            self.weather_service.home_name = context.user_data[chat_id]["domotic_info"]["home_name"]
            self.home_service.city = context.user_data[chat_id]["domotic_info"]["city_name"]
            self.home_service.home_name = context.user_data[chat_id]["domotic_info"]["home_name"]
            self.utils.check_last_and_delete(update, context, message_sent)
            return bot_states.LOGGED
        else:
            context.user_data[chat_id]["logged"] = False
            if context.user_data[chat_id]["tries"] >= self.config["telegram"]["max_tries"]:
                context.user_data[chat_id]["banned"] = True
            else:
                context.user_data[chat_id]["tries"] = context.user_data[chat_id]["tries"] + 1
            # User banned
            if context.user_data[chat_id]["banned"]:
                message_sent = context.bot.send_message(chat_id, text="😢 You are banned. Bye Bye")
                self.utils.check_last_and_delete(update, context, message_sent)
                return ConversationHandler.END
            else:
                message_sent = context.bot.send_message(chat_id, text="❌ Authentication failed.\nSend me your "
                                                                      "credentials again: <username>:<password>")
                self.utils.check_last_and_delete(update, context, message_sent)
            return bot_states.CREDENTIALS

    def show_logged_menu(self, update, context):
        self.utils.check_last_and_delete(update, context, None)
        update.message.delete()
        keyboard = [[InlineKeyboardButton(text="Find Weather Report", callback_data=str(bot_events.W_REPORT_CLICK))],
                    [InlineKeyboardButton(text="Find Home Report", callback_data=str(bot_events.H_REPORT_CLICK))],
                    [InlineKeyboardButton(text="Logout", callback_data=str(bot_events.LOGOUT_CLICK))],
                    [InlineKeyboardButton(text="❌", callback_data=str(bot_events.EXIT_CLICK))]
                    ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        update.message.reply_text(text="Menu", reply_markup=reply_markup)
        return bot_states.LOGGED

    @staticmethod
    def exit(update: Update, _):
        update.callback_query.answer()
        update.effective_message.delete()
        return bot_states.LOGGED

    def logout(self, update: Update, context):
        update.callback_query.answer()
        chat_id = update.effective_chat.id
        context.user_data[chat_id]["logged"] = False
        update.effective_message.delete()
        return self.start(update, context)
