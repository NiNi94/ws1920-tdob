# Selection Level 1 Menu Event
W_REPORT_CLICK, H_REPORT_CLICK, LOGOUT_CLICK, EXIT_CLICK, BACK_CLICK, BACK_W_CLICK, BACK_H_CLICK = map(chr,
                                                                                                       range(20, 27))
# Selection Level 2 Settings Event
TOGGLE_CLICK, LOG_CLICK, FACES_CLICK, SECONDS_CLICK, PERCENTAGE_CLICK = map(chr, range(10, 15))
