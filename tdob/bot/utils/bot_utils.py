import logging
import os

from telegram import Bot
from telegram.ext import CallbackContext

logger = logging.getLogger(os.path.basename(__file__))


class BotUtils:
    def __init__(self, config, bot: Bot):
        # Constructor
        self.config = config
        self.bot = bot

    @staticmethod
    def init_user(_, context: CallbackContext, chat_id, username):
        if chat_id not in context.user_data:
            context.user_data[chat_id] = dict()
            context.user_data[chat_id]["domotic_info"] = dict()
            context.user_data[chat_id]["username"] = username
            context.user_data[chat_id]["tries"] = 1
            context.user_data[chat_id]["logged"] = False
            context.user_data[chat_id]["banned"] = False

    @staticmethod
    def get_user_infos_message(context: CallbackContext, chat_id, username) -> str:
        home_name = context.user_data[chat_id]["domotic_info"]["home_name"]
        city_name = context.user_data[chat_id]["domotic_info"]["city_name"]
        sensors = context.user_data[chat_id]["domotic_info"]["sensors"]
        return "Inferred infos:\n" \
               "User: {}\n" \
               "Home name: {}\n" \
               "City name: {}\n" \
               "Sensors installed: {}".format(username, home_name, city_name, sensors)

    @staticmethod
    def check_last_and_delete(_, context, message):
        if "last_message" in context.user_data and message is not None:
            context.user_data["last_message"].delete()
            context.user_data["last_message"] = message
        elif "last_message" in context.user_data and message is None:
            context.user_data["last_message"].delete()
            del context.user_data["last_message"]
        elif "last_message" not in context.user_data and message is not None:
            context.user_data["last_message"] = message
        else:
            pass  # message not present or not passed
