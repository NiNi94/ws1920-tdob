import logging
from datetime import timedelta

import rdflib
from owlready2 import os, set_datatype_iri, get_ontology, sync_reasoner_pellet
from rdflib import XSD, Literal, URIRef
from rdflib.plugins.sparql import prepareQuery

from utils import utils

logger = logging.getLogger(os.path.basename(__file__))
set_datatype_iri(float, "http://www.w3.org/2001/XMLSchema#float")


class OwlManager:
    def __init__(self, config):
        self.config = config
        self.dc = get_ontology("file://{}".format(self.config["ontology"]["onto_path"])).load()
        self.update_reasoner()
        self.get_weather_condition("luca", 12300)

    def save_inferred(self):
        self.dc.save(file="{}".format(self.config["ontology"]["inferred_path"]), format="rdfxml")

    def update_reasoner(self):
        with self.dc:
            sync_reasoner_pellet(infer_property_values=True, infer_data_property_values=True)
            self.save_inferred()

    def check_user_credentials(self, username, password) -> bool:
        graph = self.dc.world.as_rdflib_graph()
        q = prepareQuery(
            """
            PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT ?password
            WHERE {
                ?user dc:hasPassword ?password.
                ?user dc:hasUsername ?username.
            }
            LIMIT 1
            """
        )
        username_literal = rdflib.term.Literal(username)
        for row in graph.query(q, initBindings={'username': username_literal}):
            if row[0].value == password:
                return True
        return False

    def get_home_name(self, username) -> str:
        graph = self.dc.world.as_rdflib_graph()
        q = prepareQuery(
            """
            PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT ?home_name
            WHERE {
                ?home dc:hasName ?home_name.
                ?home dc:inhabitedBy ?person.
                ?person dc:hasUsername ?username.
            }
            LIMIT 1
            """
        )
        username_literal = rdflib.term.Literal(username)
        for row in graph.query(q, initBindings={'username': username_literal}):
            return row[0].value

    def get_city_name(self, username) -> str:
        graph = self.dc.world.as_rdflib_graph()
        q = prepareQuery(
            """
            PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT ?city_name
            WHERE {
            ?location dc:hasName ?city_name.
            ?home dc:hasLocation ?location.
            ?home dc:inhabitedBy ?person.
            ?person dc:hasUsername ?username.
            }
            LIMIT 1
            """
        )
        username_literal = rdflib.term.Literal(username)
        for row in graph.query(q, initBindings={'username': username_literal}):
            return row[0].value

    def get_sensors_name(self, username) -> []:
        graph = self.dc.world.as_rdflib_graph()
        q = prepareQuery(
            """
            PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT DISTINCT ?sensor_name
            WHERE {
            ?sensor dc:hasName ?sensor_name.
            ?home dc:hasInstalledSensor ?sensor.
            ?home dc:inhabitedBy ?person.
            ?person dc:hasUsername ?username.
            }
            """
        )
        sensors = []
        username_literal = rdflib.term.Literal(username)
        for row in graph.query(q, initBindings={'username': username_literal}):
            sensors.append(row[0].value)
        return sensors

    def get_home_condition(self, username, days) -> str:
        dt, timestamp = utils.get_datetime()
        back_date = dt - timedelta(days=days + 1)
        forward_date = back_date + timedelta(days=1)
        graph = self.dc.world.as_rdflib_graph()
        q = """
            PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT DISTINCT ?date ?condition
            WHERE {
            ?state dc:hasHomeCondition ?condition.
            ?report dc:createdAt ?date.
            ?state dc:refersToHomeReport ?report.
            ?home dc:hasHomeReport ?report.
            ?home dc:inhabitedBy ?person.
            ?person dc:hasUsername ?username.
            FILTER (?date > ?back_date && ?date < ?forward_date)
            }
            """
        conditions = ""
        username_literal = rdflib.term.Literal(username)
        literal_back_date = Literal(back_date, datatype=XSD.dateTime)
        literal_forward_date = Literal(forward_date, datatype=XSD.dateTime)
        for row in graph.query(q, initBindings={'username': username_literal, 'back_date': literal_back_date,
                                                'forward_date': literal_forward_date}):
            conditions += "{}|{}\n".format(str(row[0].value).split("+")[0], str(row[1]).split("#")[1])
        uri: URIRef
        return conditions

    def get_weather_condition(self, username, days) -> str:
        dt, timestamp = utils.get_datetime()
        back_date = dt - timedelta(days=days + 1)
        forward_date = back_date + timedelta(days=1)
        graph = self.dc.world.as_rdflib_graph()
        q = """
            PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT DISTINCT ?date ?condition
            WHERE {
            ?state dc:hasWeatherCondition ?condition.
            ?report dc:createdAt ?date.
            ?state dc:refersToWeatherReport ?report.
            ?home dc:hasWeatherReport ?report.
            ?home dc:inhabitedBy ?person.
            ?person dc:hasUsername ?username.
            FILTER (?date > ?back_date && ?date < ?forward_date)
            }
            """
        conditions = ""
        username_literal = rdflib.term.Literal(username)
        literal_back_date = Literal(back_date, datatype=XSD.dateTime)
        literal_forward_date = Literal(forward_date, datatype=XSD.dateTime)
        for row in graph.query(q, initBindings={'username': username_literal, 'back_date': literal_back_date,
                                                'forward_date': literal_forward_date}):
            conditions += "{}|{}\n".format(str(row[0].value).split("+")[0], str(row[1]).split("#")[1])
        uri: URIRef
        return conditions

    def insert_home_report(self, home: str, time: str, temp: float, humidity: float):
        logger.info("Home info to insert:\n home: {} | time: {} | temp: {} | humidity: {}".format(home, time, temp,
                                                                                                  humidity))
        dt, timestamp = utils.get_datetime()
        timestamp = timestamp.replace(":", ".")
        # Create report
        report = self.dc.HomeReport("H-Report.{}".format(timestamp))
        report.createdAt = dt
        # Weather state
        state = self.dc.HomeState("H-State.{}".format(timestamp))
        report.reportsHomeState = state
        # Temp
        h_temp = self.dc.HomeTemperature("H-Temp.{}".format(timestamp))
        h_temp.hasValue = temp
        state.hasHomeTemperature = h_temp
        # Humidity
        h_humidity = self.dc.HomeHumidity("H-Humidity.{}".format(timestamp))
        h_humidity.hasValue = humidity
        state.hasHomeHumidity = h_humidity
        # Home
        self.dc.luca_home.hasHomeReport.append(report)
        self.update_reasoner()

    def insert_weather_report(self, home: str, time: str, temp: float, humidity: float, wind: float,
                              precipitation: float, cloud_cover: int):
        logger.info("Weather info to insert:\n home: {} | time: {} | temp: {} | humidity: {} | wind: {} | rain: {} | "
                    "cloud: {}".format(home, time, temp, humidity, wind, precipitation, cloud_cover))
        dt, timestamp = utils.get_datetime()
        timestamp = timestamp.replace(":", ".")
        # Create report
        report = self.dc.CurrentWeatherReport("W-Report.{}".format(timestamp))
        report.createdAt = dt
        # Weather state
        state = self.dc.WeatherState("W-State.{}".format(timestamp))
        report.reportsWeatherState = state
        # state.refersToWeatherReport = report
        # Temp
        w_temp = self.dc.Temperature("W-Temp.{}".format(timestamp))
        w_temp.hasValue = temp
        state.hasExteriorTemperature = w_temp
        # Humidity
        w_humidity = self.dc.Humidity("W-Humidity.{}".format(timestamp))
        w_humidity.hasValue = humidity
        state.hasHumidity = w_humidity
        # Wind
        w_wind = self.dc.Wind("W-Wind.{}".format(timestamp))
        w_wind.hasSpeed = wind
        state.hasWind = w_wind
        # Rain
        w_precipitation = self.dc.Precipitation("W-Precipitation.{}".format(timestamp))
        w_precipitation.hasIntensity = float(0)
        state.hasPrecipitation = w_precipitation
        # Cloud
        w_cloud = self.dc.CloudCover("W-Cloud.{}".format(timestamp))
        w_cloud.hasCloudCoverValue = cloud_cover
        state.hasCloudCover = w_cloud
        # Home
        self.dc.luca_home.hasWeatherReport.append(report)
        self.update_reasoner()
