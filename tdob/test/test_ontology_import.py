from unittest import TestCase

from owlready2 import get_ontology


class TestOntologyImport(TestCase):
    def test_import(self):
        onto = get_ontology("file://../../ontologies/DomoticOntology.owl").load()
        self.assertIsNot(list(onto.classes()), [])
