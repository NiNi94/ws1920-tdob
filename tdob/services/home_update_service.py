import logging
import os

from requests import get

from ontology.owl_manager import OwlManager
from services.update_service import UpdateService

logger = logging.getLogger(os.path.basename(__file__))


class HomeUpdateService(UpdateService):
    def __init__(self, config, ontology_manager: OwlManager, endpoint: str, token: str, update_frequency: int):
        super().__init__(ontology_manager, endpoint, token, update_frequency)
        self.config = config
        self.city: str = ""

    def update(self):
        if self.home_name and self.city:  # If present i can call endpoint
            headers = {
                "Authorization": "Bearer {}".format(self.token),
                "content-type": "application/json",
            }

            temp = get(self.endpoint + self.config["domotic_api"]["temperature"], headers=headers)
            humidity = get(self.endpoint + self.config["domotic_api"]["humidity"], headers=headers)
            self.ontology_manager.insert_home_report(self.home_name, self.get_timestamp(), float(temp.json()["state"]),
                                                     float(humidity.json()["state"]))
