import logging
import os
from abc import abstractmethod
from datetime import datetime, timezone
from threading import Thread
from time import sleep

from ontology.owl_manager import OwlManager

logger = logging.getLogger(os.path.basename(__file__))


class UpdateService:
    def __init__(self, ontology_manager: OwlManager, endpoint: str, token: str, update_frequency: int):
        self.ontology_manager: OwlManager = ontology_manager
        self.thread = Thread(target=self.thread_loop)
        self.endpoint: str = endpoint
        self.token: str = token
        self.update_frequency: int = update_frequency
        self.stop = False
        self.owl_manager: OwlManager
        self.home_name: str = ""

    @property
    def owl_manager(self):
        return self.owl_manager

    @owl_manager.setter
    def owl_manager(self, value):
        self.owl_manager = value

    def thread_loop(self):
        while not self.stop:
            self.update()
            sleep(3600 / self.update_frequency)

    @staticmethod
    def get_timestamp() -> str:
        utc_dt = datetime.now(timezone.utc)  # UTC time
        dt = utc_dt.astimezone()  # local time
        timestamp = dt.strftime('%Y-%m-%dT%H:%M:%SZ')
        return timestamp

    @abstractmethod
    def update(self):
        raise NotImplementedError
