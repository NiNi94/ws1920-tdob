import logging
import os

import requests

from ontology.owl_manager import OwlManager
from services.update_service import UpdateService

logger = logging.getLogger(os.path.basename(__file__))


class WeatherUpdateService(UpdateService):
    def __init__(self, ontology_manager: OwlManager, endpoint: str, token: str, update_frequency: int):
        super().__init__(ontology_manager, endpoint, token, update_frequency)
        self.city: str = ""

    def update(self):
        if self.home_name and self.city:  # If present i can call endpoint
            r = requests.get("{}q={}&units=metric&appid={}".format(self.endpoint, self.city, self.token))
            data = r.json()
            self.send_data(data)
            # with open('Cesena.json', 'r', encoding='utf-8') as f:
            #    data = json.load(f)
            #    self.send_data(data)

    def send_data(self, data: dict):
        rain = 0.0
        if "rain" in data:
            rain = data["rain"]["1h"]
        self.ontology_manager.insert_weather_report(self.home_name, self.get_timestamp(), float(data["main"]["temp"]),
                                                    float(data["main"]["humidity"]), float(data["wind"]["speed"]),
                                                    float(rain),
                                                    int(int(data["clouds"]["all"]) / 10))
