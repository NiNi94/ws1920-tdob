import logging
import os

from bot import telegram_bot
from ontology import owl_manager
from services.home_update_service import HomeUpdateService
from services.weather_update_service import WeatherUpdateService
from utils import utils

if __name__ == '__main__':
    # WORKING DIRECTORY
    abspath = os.path.abspath(__file__)
    d_name = os.path.dirname(abspath)
    os.chdir(d_name)
    # INIT
    utils.init_logger()
    logger = logging.getLogger(os.path.basename(__file__))
    config = utils.load_yaml("../config.yaml")
    logger.info("Configuration loaded")

    # ONTOLOGY
    ontology_manager = owl_manager.OwlManager(config)

    # SERVICES
    weather_config = config["weather_api"]
    home_config = config["domotic_api"]
    weather_service = WeatherUpdateService(ontology_manager, weather_config["endpoint"], weather_config["token"], 100)
    home_service = HomeUpdateService(config, ontology_manager, home_config["endpoint"], home_config["token"], 600)
    weather_service.thread.start()
    home_service.thread.start()

    # BOT
    telegram_bot = telegram_bot.TelegramBot(config, ontology_manager, weather_service, home_service)
    telegram_bot.start_polling()
