setuptools==50.3.2
PyYAML==5.3.1
requests==2.25.0
Owlready2==0.25
rdflib==5.0.0
python-telegram-bot==13.0