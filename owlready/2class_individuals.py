import types

from owlready2 import get_ontology, Thing, destroy_entity

onto = get_ontology("http://test.org/onto.owl")


# Associate the namespace to the class
class Drugs(Thing):
    namespace = onto


print("Onto classes: ", list(onto.classes()))

# Alternative
onto = get_ontology("http://test.org/onto.owl")

with onto:
    class Drug(Thing):
        pass
print("Onto classes: ", list(onto.classes()))
print("Class iri:", Drug.iri)


# Subclasses
class DrugAssociation(Drug):
    pass


print("Is_a, can modify classes:", DrugAssociation.is_a)
print("Ancestors:", DrugAssociation.ancestors())

# Dynamic classes

types.new_class("Dopamine", bases=(Drug,))
print("Classes:", list(onto.classes()))

# Equivalent
print("Equivalent:", Drug.equivalent_to)

# Individuals
my_drug = Drug("my_drug")
print("Instance name:", my_drug.name)
print("Instance iri", my_drug.iri)

unnamed_drug = Drug()
print("Instance iri:", unnamed_drug.iri)


class ActivePrinciple(Thing):
    namespace = onto


my_drug2 = Drug("my_drug2", namespace=onto, has_for_active_principle=[ActivePrinciple])
print("Instance available:", my_drug2)

print(list(Drug.instances()))
print("Onto classes:", list(onto.classes()))


# Multi-Class individual
class BloodBasedProduct(Thing):
    ontology = onto


a_blood_based_drug = Drug("Blood_based_drug")
a_blood_based_drug.is_a.append(BloodBasedProduct)
print("Classed:", a_blood_based_drug.is_a)

# Destroy
print("Individuals:", list(onto.individuals()))
destroy_entity(a_blood_based_drug)
print("Individuals:", list(onto.individuals()))
