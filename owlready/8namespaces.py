from owlready2 import get_ontology, Thing

go = get_ontology("http://purl.obolibrary.org/obo/go.owl").load()

print(go.GO_0000001)  # Not in the right namespace
obo = go.get_namespace("http://purl.obolibrary.org/obo/")
print(obo.GO_0000001)  # Not in the right namespace

onto = get_ontology("http://test.org/onto/")
namespace = onto.get_namespace("http://test.org/onto/pharmaco")


class Drug(Thing):
    namespace = namespace

# In the example above, the Drug Class IRI is “http://test.org/pharmaco/Drug”, but the Drug Class belongs to the
# ‘http://test.org/onto’ ontology.
