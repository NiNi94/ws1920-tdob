from owlready2 import Thing, get_ontology, FunctionalProperty

onto = get_ontology("http://test.org/onto.owl")
with onto:
    class Drug(Thing):
        def get_per_tablet_cost(self):
            return self.cost / self.number_of_tablets

    class has_for_cost(Drug >> float, FunctionalProperty):
        python_name = "cost"

    class has_for_number_of_tablets(Drug >> int, FunctionalProperty):
        pyhon_name = "number_of_tablets"

my_drug = Drug(cost=10.0, number_of_tablets=5)
print(my_drug.get_per_tablet_cost())

# Forward declarations
with onto:
    class ActivePrinciple(Thing):
        pass

with onto:
    class Drug(Thing):
        pass

    class has_for_active_principle(Drug >> ActivePrinciple):
        pass

    class Drug(Thing):  # Extends the previous definition of Drug
        is_a = [has_for_active_principle.some(ActivePrinciple)]
