import json

import requests

r = requests.get(
    "http://api.openweathermap.org/data/2.5/weather?q=Cesena&units=metric&appid=TOKEN")
with open('prova.json', 'w', encoding='utf-8') as f:
    json.dump(r.json(), f, ensure_ascii=False, indent=4)
