from rdflib import Graph

# default_world.set_backend(filename="ciao.sqlite3")
# default_world.save()

# WORLDS used to load more version of the same ontology
# my_world = World()
# my_second_world = World(filename="bubu.sqlite3")

# onto = my_world.get_ontology("http://test.org/onto/")
# var = my_world["http://test.org/onto/my_iri"]

# sync_reasoner_pellet(my_world)

g = Graph()
g.parse("http://www.w3.org/People/Berners-Lee/card")
res = g.query(
    """
    PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
    SELECT ?name
    WHERE {
        ?person foaf:name ?name .
    }
    """)
for row in res:
    print(row)
