from datetime import datetime, timezone

from owlready2 import get_ontology

onto = get_ontology("file://../ontologies/DomoticOntology.owl").load()

utc_dt = datetime.now(timezone.utc)  # UTC time
dt = utc_dt.astimezone()  # local time
print(dt)
print(list(onto.classes()))
