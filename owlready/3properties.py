from owlready2 import get_ontology, Thing, ObjectProperty, DataProperty, FunctionalProperty

onto = get_ontology("http://test.org/onto.owl")

# Define properties

with onto:
    class Drug(Thing):
        pass

    class Ingredient(Thing):
        pass

    class has_for_ingredient(ObjectProperty):
        domain = [Drug]
        range = [Ingredient]

with onto:
    class has_for_ingredients(Drug >> Ingredient):
        pass

# Property instances
my_drug = Drug("my_drug")
acetaminophen = Ingredient("acetaminophen")
my_drug.has_for_ingredient = [acetaminophen]
print("Classes:", list(onto.classes()))
print("Obj props:", list(onto.object_properties()))
print("Is a:", my_drug.is_a)
print("Search instance for prop:", onto.search(has_for_ingredient="*"))
print(my_drug.has_for_ingredient)
codeine = Ingredient("codeine")
my_drug.has_for_ingredient.append(codeine)
print(my_drug.has_for_ingredient)

# Data property
with onto:
    class has_for_synonym(DataProperty):
        range = [str]

acetaminophen.has_for_synonym = ["acetaminophen", "paracetamols"]
print("Synonym:", acetaminophen.has_for_synonym)

with onto:
    class has_for_synonyms(Thing >> str):
        pass

# Inverse
with onto:
    class is_ingredient_of(ObjectProperty):
        domain = [Ingredient]
        range = [Drug]
        inverse_property = has_for_ingredient

my_drug2 = Drug("my_drug2")
aspirin = Ingredient("aspirin")
my_drug2.has_for_ingredient.append(aspirin)
print("Ingredient:", my_drug2.has_for_ingredient)

print("Is Ingredient:", aspirin.is_ingredient_of)
aspirin.is_ingredient_of = []
print("Is Ingredient:", aspirin.is_ingredient_of)

# Functional
with onto:
    class has_for_cost(DataProperty, FunctionalProperty):
        domain = [Drug]
        range = [float]
my_drug.has_for_cost = 4.2
print("Cost:", my_drug.has_for_cost)
print("Drug 2 cost:", my_drug2.has_for_cost)

# Inverse functional
with onto:
    class prop(ObjectProperty):
        pass

    class A(Thing):
        pass

    class B(Thing):
        is_a = [prop.max(1)]

a = A("a")
b = B("b")
print("Prop in A:", a.prop)
print("Prop in B", b.prop)

# Sub-property
with onto:
    class ActivePrinciple(Ingredient):
        pass

    class has_for_active_principle(has_for_ingredient):
        domain = [Drug]
        range = [ActivePrinciple]

# Aliases for property
has_for_ingredient.python_name = "ingredients"
my_drug3 = Drug("my_drug3")
cetirizin = Ingredient("cetirizin")
my_drug3.ingredients = [cetirizin]
