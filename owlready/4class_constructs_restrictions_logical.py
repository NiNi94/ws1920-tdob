from owlready2 import get_ontology, Not, Thing, OneOf, Inverse, ConstrainedDatatype

onto = get_ontology("http://test.org/onto.owl")
with onto:
    class Drug(Thing):
        pass

    class ActivePrinciple(Thing):
        pass

    class has_for_active_principle(Drug >> ActivePrinciple):
        pass

    class Placebo(Drug):
        equivalent_to = [Drug & Not(has_for_active_principle.some(ActivePrinciple))]

    class DrugAssociation(Drug):
        equivalent_to = [Drug & has_for_active_principle.min(2, ActivePrinciple)]

# RESTRICTIONS:
'''
some : Property.some(Range_Class)
only : Property.only(Range_Class)
min : Property.min(cardinality, Range_Class)
max : Property.max(cardinality, Range_Class)
exactly : Property.exactly(cardinality, Range_Class)
value : Property.value(Range_Individual / Literal value)
'''

# OPERATORS
'''
‘&’ : and operator (intersection). For example: Class1 & Class2
‘|’ : or operator (union). For example: Class1 | Class2
Not() : not operator (negation or complement). For example: Not(Class1)
'''

# One-Of
with onto:
    class DrugForm(Thing):
        pass

tablet = DrugForm()
capsule = DrugForm()
injectable = DrugForm()
pomade = DrugForm
DrugForm.is_a.append(OneOf([tablet, capsule, injectable, pomade]))

# Inverse
is_contained_in = Inverse(has_for_active_principle)

# CONSTRAINED DATATYPE
'''
length
min_length
max_length
pattern
white_space
max_inclusive
max_exclusive
min_inclusive
min_exclusive
total_digits
fraction_digits
'''
ConstrainedDatatype(int, min_inclusive=0, max_inclusive=20)
ConstrainedDatatype(str, max_length=100)

# Chain
# PropertyChain([prop1, prop2])
