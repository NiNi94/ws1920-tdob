from owlready2 import get_ontology, sync_reasoner_pellet
from rdflib import Graph

onto = get_ontology(base_iri="http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl")
onto.load()

with onto:
    class SausagePizza(onto.Pizza):
        equivalent_to = [
            onto.Pizza
            & (onto.has_topping.some(onto.MeatTopping))
        ]

        @staticmethod
        def eat(): print("Salcicciaaa!")

print(onto.SausagePizza)

test_pizza = onto.SausagePizza("sausage_instance")
test_pizza.has_topping = [onto.CheeseTopping(),
                          onto.TomatoTopping(),
                          onto.MeatTopping()]
print(onto.Pizza("sausage_instance"))
onto.save("onto.owl")

sync_reasoner_pellet()
print(test_pizza.__class__)
test_pizza.eat()
world = onto.world
# world.set_backend(filename="prova.sqlite3")
# world.save()
graph = world.as_rdflib_graph()

g = Graph()
g.parse("http://www.w3.org/People/Berners-Lee/card")
res = g.query(
    """
    PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
    SELECT ?name
    WHERE {
        ?person foaf:name ?name .
    }
    """)
for row in res:
    print(row)
