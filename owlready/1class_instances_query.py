from time import sleep

from owlready2 import sync_reasoner_pellet, IRIS, get_ontology

onto = get_ontology(base_iri="http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl")
onto.load()


# Create a new class
class VegPizza(onto.Pizza):
    equivalent_to = [
        onto.Pizza
        & (onto.has_topping.some(onto.MeetTopping)
           | onto.has_topping.some(onto.FishTopping)
           )]


# Create an instance
test_pizza = onto.Pizza("test_pizza")
test_pizza.has_topping = [onto.CheeseTopping(),
                          onto.TomatoTopping()]

# Print all classes
print(list(onto.classes()))
print(list(onto.individuals()))
# Print type
print("test_pizza:", test_pizza.__class__)
# Reasoner
sync_reasoner_pellet()
sleep(1)
# Infer
print("Inferred:", test_pizza.__class__)  # Inferred

# IRI access
var = IRIS["http://www.lesfleursdunormal.fr/static/_downloads/pizza_onto.owl#Pizza"]
print("Access through iri:", var)

# Query
print("Search topping:", onto.search(iri="*Topping"))
print("Search *pizza:", onto.search(iri="*pizza"))
print("All has_topping instances:", onto.search(has_topping="*"))
print("All Veg instances:", onto.search(type=VegPizza))
print("Subclass of Pizza:", onto.search(subclass_of=onto.Pizza))
print("All is_a:", onto.search(is_a=onto.Pizza))
print("One is_a:", onto.search_one(is_a=onto.Pizza))

# Saving
# onto.save(file = "filename or fileobj", format = "rdfxml")
