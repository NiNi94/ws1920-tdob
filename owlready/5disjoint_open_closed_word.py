from owlready2 import get_ontology, Thing, AllDifferent, AllDisjoint, OneOf, close_world

onto = get_ontology("http://test.org/onto.owl")

with onto:
    class Drug(Thing):
        pass

    class ActivePrinciple(Thing):
        pass

    AllDisjoint([Drug, ActivePrinciple])

acetaminophen = ActivePrinciple("acetaminophen")
aspirin = ActivePrinciple("aspirin")
AllDifferent([acetaminophen, aspirin])

print("Disjoint:", list(Drug.disjoints()))

# Close
with onto:
    class has_for_active_principle(Drug >> ActivePrinciple):
        pass
my_acetaminophen_drug = Drug(name="acetaminophen", namespace=onto, kwarhas_for_active_principle=[acetaminophen])
my_acetaminophen_drug.is_a.append(has_for_active_principle.only(OneOf([acetaminophen])))

# Close World
close_world(my_acetaminophen_drug)
close_world(onto)  # Work with ontologies
