from owlready2 import Thing, get_ontology, Not, AllDifferent, sync_reasoner_pellet, close_world

onto = get_ontology("http://test.org/onto.owl")

with onto:
    class Drug(Thing):
        def take(self): print("I took a drug")

    class ActivePrinciple(Thing):
        pass

    class has_for_active_principle(Drug >> ActivePrinciple):
        # python_name = "active_principles"
        pass

    class Placebo(Drug):
        equivalent_to = [Drug & Not(has_for_active_principle.some(ActivePrinciple))]

        def take(self): print("I took a placebo")

    class SingleActivePrincipleDrug(Drug):
        equivalent_to = [Drug & has_for_active_principle.exactly(1, ActivePrinciple)]

        def take(self): print("I took a drug with a single active principle")

    class DrugAssociation(Drug):
        equivalent_to = [Drug & has_for_active_principle.min(2, ActivePrinciple)]

        def take(self): print("I took a drug with {} active principles".format(self.has_for_active_principle))

acetaminophen = ActivePrinciple("acetaminophen")
amoxicillin = ActivePrinciple("amoxicillin")
clavulanic_acid = ActivePrinciple("clavulanic_acid")

AllDifferent([acetaminophen, amoxicillin, clavulanic_acid])

drug1 = Drug(has_for_active_principle=[acetaminophen])
drug2 = Drug(has_for_active_principle=[amoxicillin, clavulanic_acid])
drug3 = Drug(has_for_active_principle=[])

close_world(Drug)

with onto:
    sync_reasoner_pellet()

drug1.take()
drug2.take()
drug3.take()
