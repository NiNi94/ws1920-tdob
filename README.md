# Telegram Domotic Ontology bot

## This telegram bot allow a user to:

- Authenticate using a Username through the DomoticOntology.owl
- Ask for home informations: city, name, installed sensors
- Retrieve weather conditions from OpenWeatherMap Api
- Retrieve home conditions from different sensors

## How to set Ontology informations
- Open **DomoticOntology.owl** 
- Add a **Person** with username and password
- Add a **Home** associated to the username
- Add a **Location** and associate home
- Add **sensors**

## How to set yaml configuration
- Crate a **config.yaml** file form config_example.yaml
- Create a **telegram bot** and insert Api token
- Register on **Openweathermap** and insert Api token
- If you have **Home Assistant** insert Api token

You can disable an **UpdateService** agent simply commenting it in **main.py**

## Architecture
![Architecture](https://bitbucket.org/NiNi94/ws1920-tdob/raw/6f733dc2481358b4687336e97cc22a2531b53d14/doc/images/DomoticOntology.png)
