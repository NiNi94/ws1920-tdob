PREFIX dc: <http://www.unibo.it/../DomoticOntology.owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
SELECT ?home_name
WHERE {
    ?home dc:hasName ?home_name.
    ?home dc:inhabitedBy ?person.
    ?person dc:hasUsername ?username.
}
LIMIT 1