PREFIX dc: <http://www.unibo.it/../DomoticOntology.owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
SELECT DISTINCT ?sensor_name
WHERE {
    ?sensor dc:hasName ?sensor_name.
    ?home dc:hasInstalledSensor ?sensor.
    ?home dc:inhabitedBy ?person.
    ?person dc:hasUsername ?username.
}