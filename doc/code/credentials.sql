PREFIX dc: <http://www.unibo.it/../DomoticOntology.owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
SELECT ?password
WHERE {
    ?user dc:hasPassword ?password.
    ?user dc:hasUsername ?username.
}
LIMIT 1