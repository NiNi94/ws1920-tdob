from owlready2 import sync_reasoner_pellet, IRIS, get_ontology
onto = get_ontology("http://www.onto.com/pizza_onto.owl")
onto.load()
# Create a new class
class VegPizza(onto.Pizza):
    equivalent_to = [
        onto.Pizza
        & (onto.has_topping.some(onto.MeetTopping)
           | onto.has_topping.some(onto.FishTopping)
           )]
# Create an instance
test_pizza = onto.Pizza("test_pizza")
test_pizza.has_topping = [onto.CheeseTopping(),
                          onto.TomatoTopping()]
# Inference
print("Pizza:", test_pizza.__class__) => test_pizza: pizza_onto.Pizza
with onto: 
  sync_reasoner_pellet(infer_property_values=True)
print("Inferred:", test_pizza.__class__)  => Inferred: pizza_onto.VegPizza