def check_user_credentials(self, username, password) -> bool:
    graph = self.dc.world.as_rdflib_graph()
    q = prepareQuery(
        """
        PREFIX dc: <http://www.unibo.it/lucagiulianini/ontologies/DomoticOntology.owl#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT ?password
        WHERE {
            ?user dc:hasPassword ?password.
            ?user dc:hasUsername ?username.
        }
        LIMIT 1
        """
    )
    username_literal = rdflib.term.Literal(username)
    for row in graph.query(q, initBindings={'username': username_literal}):
        if row[0].value == password:
            return True
    return False