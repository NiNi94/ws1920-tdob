PREFIX dc: <http://www.unibo.it/../DomoticOntology.owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
SELECT ?city_name
WHERE {
    ?location dc:hasName ?city_name.
    ?home dc:hasLocation ?location.
    ?home dc:inhabitedBy ?person.
    ?person dc:hasUsername ?username.
}
LIMIT 1