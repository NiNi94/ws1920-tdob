PREFIX dc: <http://www.unibo.it/../DomoticOntology.owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
SELECT DISTINCT ?date ?condition
WHERE {
    ?state dc:hasHomeCondition ?condition.
    ?report dc:createdAt ?date.
    ?state dc:refersToHomeReport ?report.
    ?home dc:hasHomeReport ?report.
    ?home dc:inhabitedBy ?person.
    ?person dc:hasUsername ?username.
    FILTER (?date > ?back_date && ?date < ?forward_date)
}