% -*- root: ../main.tex -*-
\chapter{Ontologie}
Per indagare la possibilità che un'ontologia già esistente si qualifichi per essere utilizzata come base per \texttt{DomoticOntology}, questa sezione presenta una \textbf{selezione di ontologie} che sono state progettate per coprire il dominio dei dati meteorologici, ambientali e IOT. Nel caso in cui non sia possibile identificare un'ontologia adeguata, vengono analizzati i \textbf{vantaggi} e gli \textbf{svantaggi} delle ontologie discusse al fine di evitare di includere le loro carenze all'interno di \texttt{DomoticOntology} e di beneficiare dei loro vantaggi.

\section{Ontologie per la Sensoristica}
Relativamente all'aspetto sensoristico esistono una serie di ontologie estremamente \textbf{robuste} capaci di rappresentare ogni singola informazione relativa al concetto di sensore e alle \textbf{proprietà} ad esso correlate. Questo tipo di ontologie sono generalmente utilizzate in \textbf{ambito scientifico} e modellano \textbf{aspetti} molto \textbf{dettagliati} e avanzati relativi alla realtà rappresentata. Anche se inizialmente ciò potrebbe andare contro gli obiettivi di sviluppo e modellazione che ci si presuppone, è innegabile come questi stessi modelli, caratterizzati dall'uso di \textbf{pattern di progettazione} complessi, costituiscano un ottimo esempio di carattere \textbf{formativo}.

Le ontologie che si andranno ad analizzare sono \texttt{Semantic Sensor Web} ed \texttt{SSN}. 

\subsection{SSW: Semantic Sensor Web}
Sensor Web Enablement (\texttt{SWE}) \cite[SSW:2008]{SSW:2008} è un'iniziativa avviata dall'\textit{Open Geospatial Consortium} (OGC) per la creazione di \textbf{reti di sensori basate su tecnologie Web}. Sia i sensori che i dati dei sensori archiviati devono essere rilevati, accessibili e controllati utilizzando \textbf{protocolli} e \textbf{interfacce  aperte}. SWE è quindi una suite di standard, ciascuno dei quali specifica le \textbf{codifiche} per descrivere i sensori, le \textit{osservazioni} dei sensori stessi e le \textit{definizioni} della loro interfaccia.

Esiste un numero enorme di \textbf{reti sensoristiche} in tutto il mondo che comprendono sensori per un'ampia serie di \textbf{fenomeni diversi}. Pertanto, è disponibile una \textbf{grande quantità di dati} e sorge la necessità di strutturare tali dati per consentire l'interoperabilità tra diverse reti di sensori.

Semantic Sensor Web (\texttt{SSW}) è dunque un'ontologia che si basa sulle attività \texttt{SWE} del W3C e che mira ad \textbf{annotare} i dati dei sensori con \textbf{metadati semantici} per aumentare l'\textbf{interoperabilità} e fornire informazioni contestuali essenziali per garantire l'\textbf{inferenza} di nuova conoscenza.

\subsubsection{L'Ontologia}
L'ontologia \texttt{SSW} è costruita su sette concetti di primo livello, escludendo i concetti di \textit{Location} e \textit{Time} che sono importati da altre fonti. Questi concetti di primo livello sono:
\begin{itemize}
  \item \textit{Feature:} un'astrazione di un \textbf{fenomeno}, ad esempio un evento meteorologico come una bufera di neve.
  \item \textit{Observation:} l'atto di \textbf{osservare} una proprietà o un fenomeno con l'obiettivo di determinare il valore di una proprietà.
  \item \textit{ObservationCollection:} un insieme di osservazioni.
  \item \textit{Process:} un metodo, un algoritmo o uno strumento.
  \item \textit{PropertyType:} una caratteristica di uno o più tipi di \texttt{Features}.
  \item \textit{ResultData:} una stima del valore di una proprietà generata da una procedura.
  \item \textit{UnitOfMeasurement:} un'unità di misura.
\end{itemize}

L'ontologia \texttt{SSW} include dati \textit{temporali} utilizzando \texttt{OWL-Time}, \textit{unità di misura} e \textit{dati geografici} basati sul \texttt{W3C Basic Geo}. Il concetto \texttt{WeatherObservation} include cinque sottoconcetti predefiniti progettati per mappare le osservazioni di \textit{pressione atmosferica}, \textit{precipitazione}, \textit{radiazione}, \textit{temperatura} o \textit{vento} rispettivamente. Ulteriori concetti possono essere aggiunti per l'osservazione di altri fenomeni.
L'ontologia \texttt{SSW} \textbf{non} supporta il concetto di \textbf{previsione metereologica}, ma sarebbe possibile estendere l'ontologia al fine di implementarli.

\subsection{SSN: Semantic Sensor Network}
Un altro approccio verso una rete di sensori semanticamente arricchita è un'ontologia OWL 2 creata dal gruppo W3C \textbf{Semantic Sensor Network Incubator}, la \texttt{SSN} Ontology. L'obiettivo di questa ontologia è \textbf{semplificare} la \textbf{gestione}, l'\textbf{interrogazione} e la \textbf{combinazione} di sensori e dati di osservazione da diverse reti di sensori. Essa è basata sull'ontologia \texttt{SWE} ed utilizza \texttt{DOLCE-UltraLite} come ontologia di livello superiore. 

Definisce \textbf{41 concetti} e \textbf{39 proprietà} in tutto organizzati in \textbf{dieci moduli}: 
\begin{itemize}
  \setlength\itemsep{0.1em}
  \item \textit{ConstraintBlock:} per definire le condizioni sul funzionamento di un sistema o di un sensore.
  \item \textit{Data:} per codificare qualsiasi input dai sensori.
  \item \textit{Device:} per definire dispositivi nella rete di sensori, principalmente sensori.
  \item \textit{Deployment:} per specificare la distribuzione dei dispositivi.
  \item \textit{Measuring Capability:} proprietà dei sensori, ad esempio precisione o tempo di risposta.
  \item \textit{Operating Restriction:} per definire le condizioni in cui si prevede che il sistema funzioni, ad esempio la durata delle batterie o programmi di manutenzione.
  \item \textit{PlatformSite:} entità a cui possono essere collegate altre entità - sensori e altre piattaforme.
  \item \textit{Process:} una procedura che cambia lo stato del sistema in qualche modo, richiede un input e restituisce un output.
  \item \textit{Skeleton:} per mappare il mondo reale fenomeni, le loro proprietà e le loro relazioni con i sensori.
  \item \textit{System:} per descrivere parti di infrastruttura, ad esempio l'intera rete, un componente, i suoi sottosistemi ecc.
\end{itemize}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.60\textwidth]{SSN}
  \caption{Rappresentazione visiva di SSN}
  \label{fig:SSN}
\end{figure} 


\subsection{Basic Geo}
Per gestire i dati sulla posizione geografica, il W3C Semantic Web Group ha sviluppato il \textbf{Basic Geo Vocabulary}. Questa ontologia introduce un concetto chiamatoo non \texttt{SpatialThing} e i suoi attributi \textbf{latitude}, \textbf{longitude} e \textbf{altitude} secondo il sistema di riferimento geodetico\footnote{La geodesia è una disciplina appartenente alle scienze della Terra che si occupa della misura e della rappresentazione della Terra, del suo campo gravitazionale e dei fenomeni geodinamici (spostamento dei poli, maree terrestri e movimenti della crosta).}.

\subsection{Owl Time}
Per specificare le proprietà temporali, il W3C offre \texttt{OWL-Time}, un'\textbf{ontologia temporale} in OWL. Essa definisce il concetto \texttt{TemporalEntity} il quale può assumere la forma di \texttt{Instant} o \texttt{Interval}. Questi concetti, assieme a un pacchetto \textit{minimale} di proprietà, permettono di descrivere al meglio qualsiasi concetto di tipo temporale.

\section{Ontologie per la Domotica}
Dato che l'obiettivo di questo progetto consiste nello sviluppo di un'ontologia dedicata all'acquisizione di dati nell'ambito \texttt{IOT} è importante dare uno sguardo alle principali ontologie dedicate a questo mondo in continua espansione. 

Dopo una breve ricerca è stata individuata come rappresentante di quest'ambito, l'ontologia \texttt{SAREF}:\textbf{S}mart \textbf{A}ppliances \textbf{REF}erence.

\subsection{SAREF}
L'ontologia \texttt{SAREF} ha come scopo ultimo quello di fornire una rappresentazione esaustiva relativa dell'ambito delle smart home. L'origine dell'ontologia è da ricercarsi all'interno del gruppo di ricerca belga \texttt{TNO}\footnote{TNO connects people and knowledge to create innovations that boost the
sustainable competitiveness of industry and well-being of society. TNO is a not-for-profit Research and Technology Organization (RTO)}, il quale nel \textbf{2015} ha messo in campo un enorme sforzo al fine di unificare \textbf{semantica} e \textbf{dato} in ambito \textbf{smart applicances}.

Sebbene la ricerca rappresentasse uno studio per la Commissione Europea, questa ben presto ha assunto dimensioni molto elevate e ha portato risultati molto importanti. L'ontologia sviluppata dal gruppo, infatti, permette di connettere e far interagire fra loro \textbf{dispositivi} estremamente \textbf{diversi} anche provenienti da produttori differenti. Questo risultato è stato possibile grazie ad una \textbf{modellazione} molto accurata del dominio in questione cercando di identificare tutte le tipologie di device in gioco. 

Un altro motivo del successo dello studio deriva dallo standard al quale si sono affidati i ricercatori: essi infatti hanno fatto riferimento all'architettura \texttt{M2M} \footnote{Con M2M, acronimo di Machine-to-machine, in generale ci si riferisce a tecnologie e applicazioni di telemetria e telematica che utilizzano le reti wireless.} sviluppata da \texttt{ETSI} (European Telecommunications Standard Institute).

\subsubsection{Entità}
\texttt{SAREF} si basa essenzialmente su un numero estremamente \textbf{limitato} di concetti:
\begin{itemize}
  \item \textit{Device:} un device è un oggetto intelligente o non, che è in grado di svolgere una \textbf{funzione}. Un Device può avere diverse forme e l'ontologia cerca di definire degli aspetti generici al fine di racchiudere tutte le possibili combinazioni per quanto riguarda un dispositivo.
  \item \textit{Function:} una funzione è un'attività svolta da un singolo device. Un device caratterizzato da più funzioni semplici può essere trasformato in un device capace di svolgere \textbf{compiti complessi}.
  \item \textit{Command:} ogni funzione ha associati a se uno o più comandi.
  \item \textit{State:} un device, una volta comandato, può trovarsi in vari stati.
  \item \textit{Service:} un device dotato di funzioni mette a disposizione un servizio.
\end{itemize}

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.80\textwidth]{SAREF}
  \caption{Rappresentazione visiva di SAREF}
  \label{fig:SAREF}
\end{figure} 

Come si può vedere dall'organizzazione dell'ontologia, essa pur avendo un insieme di concetti limitato permette di rappresentare \textbf{informazioni} estremamente \textbf{complesse}; questo avviene poiché le proprietà presenti garantiscono un grado elevato di \textbf{interazione} fra entità,


\section{Ontologie per il Meteo}
L'ultima tipologia analizzata riguarda le ontologie dedicate alla modellazione di condizioni metereologiche. In questo senso non esistono esempi standardizzati dal \texttt{W3C} ma solamente studi svolti da enti terzi. Questo è il caso dell'ontologia \textbf{Weather Ontology} creata da un'università austriaca con sede a Vienna, la \texttt{Tu-Wien}. L'ontologia da un primo sguardo si è subito rivelata estremamente interessante: essa modella il concetto di \textbf{condizione meteo} in modo estremamente dettagliato rimanendo comunque \textbf{molto comprensibile} e \textbf{robusta}. Nell'immagine \ref{fig:WO} possiamo vedere l'insieme di entità dedicate alla modellazione di un \textbf{fenomeno atmosferico}. 
\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.99\textwidth]{WeatherOntology}
  \caption{Rappresentazione visiva di WeatherOntology}
  \label{fig:WO}
\end{figure}

\section{Ontologia scelta}
Dopo aver descritto approfonditamente le ontologie più inerenti all'argomento di studio, è giunto il momento di effettuare una valutazione volta all'identificazione di una \textbf{base ontologica robusta} su cui impostare la propria knowledge base. Infatti, sebbene tutte le ontologie proposte forniscono una modellazione di concetti utili per lo sviluppo del progetto, è innegabile che presentino allo stesso tempo un insieme di \textbf{conoscenze superflue} che devono essere gestite.

\subsection{Inconsistenze ontologiche}
Se in un modello a \textbf{mondo chiuso}, come un database, la presenza di informazioni superflue può rappresentare una condizione accettabile, in un modello a \textbf{mondo aperto} questa condizione è da evitare a tutti i costi. Le ontologie, infatti, sono modelli di conoscenza estremamente connessi al \textbf{ragionamento logico} e necessitano di uno sforzo maggiore per essere sviluppate. Introdurre elementi superflui a un'ontologia di per sé complicata, tende a complicare la stessa di un fattore estremamente elevato portando, in situazioni estreme a situazioni di ingestibilità.

\subsection{Confronto ontologie proposte}
Al fine di effettuare una scelta ponderata si è deciso di valutare l'insieme delle ontologie proposte individuando per ognuna di esse \textbf{pro} e \textbf{contro} relativamente al contesto preso in esame. La valutazione è stata svolta analizzando un insieme di \textbf{elementi chiave} necessari nell'ambito \textbf{smart home}: \textbf{estendibilità}, \textbf{semplicità} e \textbf{robustezza}.

\begin{itemize}
  \item \texttt{SSW e SSN:} dato che \texttt{SSN} presenta un'inclusione dell'ontologia \texttt{SSW} si è deciso di prendere in esame solamente la seconda.
  \begin{itemize}
    \item \textit{Estendibilità:} l'ontologia è stata sviluppata dando grande peso al concetto di estendibilità. Essa permette di definire qualsiasi tipo di sensore e di misurazione e garantisce \textbf{piena compatibilità} rispetto all'introduzione di nuovi sensori e attuatori.
    \item \textit{Semplicità:} seppur dotata di un'ottima documentazione l'ontologia pecca di semplicità, essa rappresenta \textbf{concetti molto astratti} e ciò potrebbe generare problemi nel momento in cui si voglia definire gerarchie di entità concrete. Inoltre l'enorme mole di concetti proposti rischia di far lievitare le dimensioni dell'ontologia finale.
    \item \textit{Robustezza:} l'ontologia è estremamente robusta e \textbf{non presenta inconsistenze}. Ogni concetto è stato strutturato e pensato al meglio al fine di \textbf{modellare fedelmente} la realtà.
  \end{itemize}
  \item \texttt{SAREF:} quest'ontologia si sposta dal concetto astratto mostrato da \texttt{SSN} e si dirige verso una modellazione di un \textbf{ambito più ristretto}, quello dei sistemi \texttt{IOT}. 
  \begin{itemize}
    \item \textit{Estendibilità:} l'ontologia gode di grande estendibilità. Essa non è ai livelli proposti da \texttt{SSN} ma può essere vista come un \textbf{compromesso accettabile}.
    \item \textit{Semplicità:} anche in questo caso \textbf{predomina l'astrazione} ma dato il dominio questa stessa astrazione diventa più accettabile all'utente finale che è in grado di comprendere facilmente le varie \textbf{proprietà}. 
    \item \textit{Robustezza:} l'ontologia è molto robusta se gestita nel proprio dominio, nel momento in cui si vogliano introdurre concetti che esulano dal contesto \textbf{Smart Thing} potrebbe generare qualche incompatibilità.
  \end{itemize}
   \item \texttt{Weather Ontology:} è un'ontologia sviluppata da \textbf{terzi} e per questo motivo potrebbe presentare qualche rischio di \textbf{inconsistenza}.
  \begin{itemize}
    \item \textit{Estendibilità:} l'ontologia \textbf{non è molto estendibile} relativamente al proprio contesto ma garantisce facilmente la possibilità di aggiungere \textbf{elementi extra dominio}.
    \item \textit{Semplicità:} l'ontologia da molto peso alla sostanza; essa è basata sulla semplicità di concetti e presenta per questo motivo un \textbf{numero limitato di classi}.
    \item \textit{Robustezza:} sebbene sia un'ontologia molto semplice essa non spicca per robustezza. Alcuni \textbf{tipi di dato} e \textbf{restrizioni}, infatti, tendono a produrre \textbf{inconsistenze randomiche}: questo perché l'ontologia entra nel dominio delle \texttt{OWL-Full} ontologies caratterizzate da \textbf{indecidibilità}.
  \end{itemize}
\end{itemize}


\subsection{Scelta finale}
Valutando l'insieme di ontologie proposte, a fronte di un progetto dedicato all'ambito domotico, si è deciso di indirizzarsi verso l'ultima ontologia proposta, \textbf{Weather Ontology}. Questa scelta è stata influenzata principalmente dalla \textbf{semplicità} dell'ontologia che garantisce una \textbf{grande espansione} con concetti \textbf{extra dominio}. Le altre ontologie proposte, sebbene non siano state scelte, sono state \textbf{utilizzate come esempio} per la definizione di concetti ulteriori all'interno dell'ontologia in questione.


