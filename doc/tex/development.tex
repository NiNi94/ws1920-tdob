% -*- root: ../main.tex -*-
\chapter{Sviluppo applicativo}
Una volta completata la modellazione dell'ontologia \textbf{Domotic Ontology} è giunto il momento di testarla in un contesto reale sviluppando un applicativo dedicato. A tale scopo si è scelto di indirizzarsi verso lo sviluppo di un'\textbf{applicazione domotica} di carattere strumentale e volta a fornire \textbf{confort} all'utente. 

La descrizione della fase di sviluppo verrà introdotta nelle seguenti sezioni: inizialmente si andrà a delineare i \textbf{requisiti} fondamentali estratti e successivamente ci si focalizzerà sugli aspetti di \textbf{progettazione} e \textbf{implementativi}.

\section{Analisi}
Il processo di analisi si è svolto in maniera estremamente lineare: i \textbf{requisiti} individuati sono estremamente \textbf{semplici} e fanno riferimento ad un'\textbf{idea} molto \textbf{naive} ma allo stesso tempo \textbf{interessante}.

\subsection{Goals}
Lo scopo principe sarà quello di sviluppare una \textbf{dashboard di controllo} utilizzando una piattaforma sempre più in voga, \texttt{Telegram}. Telegram, infatti, oltre ad essere una piattaforma di messaggistica fornisce un'insieme di \textbf{funzionalità extra} dedicate allo sviluppo di semplici applicazioni interne, i \textbf{bot}. Un bot telegram ha una struttura estremamente banale, essa si compone essenzialmente di:
\begin{itemize}
  \item \textbf{Running bot:} è l'applicazione che andremo a sviluppare. Un running bot può essere di due tipologie:
  \begin{itemize}
    \item \textit{Polling bot:} come suggerisce il nome, un polling bot è un applicativo che effettua \textbf{richieste a polling} in un intervallo di tempo costante, generalmente intorno ai \texttt{10ms}. Un bot di questo tipo è \textbf{poco efficiente} dato che lavora in modalità \textbf{constant pull} ma è da preferire nei casi in cui si preferisca garantire una \textbf{maggiore sicurezza} e \textbf{semplicità di installazione}.
    \item \textit{Webhook bot:} in questo caso l'applicazione si \textbf{aggancia} (hook) al server di Telegram e in questo modo è lo stesso server ad aggiornare con \texttt{push} l'applicativo. Questa tipologia è da preferire nel caso in cui il \textbf{numero di utenti} collegati raggiunga valori estremamente \textbf{elevati} oppure nel caso in cui non ci siano problemi nell'\textbf{apertura di porte} nel router.
  \end{itemize}
  \item \textbf{Api:} sono l'\textbf{interfaccia applicativa} fornita da telegram e su cui si basa lo stesso \textbf{Running Bot}. Le Api forniscono, per esempio, funzioni di invio immagini, messaggi, ecc e permettono di sfruttare al massimo le funzionalità messe a disposizione  dall'\textbf{app di messaggistica}.
  \item \textbf{Bot conversation:} è un'istanza del bot presente all'interno di una \textbf{conversazione Telegram}. Una bot conversation è assimilabile a una conversazione classica ma presenta un insieme di \textbf{estensioni extra dedicate} (keyboard, query, ecc).
\end{itemize}

\subsection{Requisiti}
Definito l'obiettivo finale del progetto, andiamo ora ad analizzare i \textbf{requisiti base} che dovrà avere l'applicazione. Al fine di rendere il tutto più comprensibile si è deciso di racchiudere i requisiti principali all'interno di un \textbf{diagramma dei casi d'uso} fig:~\ref{fig:use_case}.

Come vediamo dal diagramma, il bot dovrà fornire queste funzionalità essenziali:
\begin{itemize}
  \item \textbf{Autenticazione:} un sistema sicuro deve mettere in campo un metodo per poter autenticare i suoi utenti. Per fare ciò è stato creato il concetto di utente all'interno dell'ontologia e sono state fornite ad esso un insieme di proprietà che ne garantiscano l'autenticazione. Al fine di garantire una \textbf{sicurezza maggiore} si può prendere in considerazione il salvataggio delle password utilizzando un approccio ad \texttt{hash} con \texttt{sale}: così facendo si complicherebbe di molto la vita a un potenziale attaccante che tentasse di accedere.
  \item \textbf{Informazioni generali:} il sistema dovrà ovviamente fornire un metodo al fine di recuperare le \textbf{informazioni di tipo domotico} come sensori installati, location ecc.
  \item \textbf{Condizioni ambientali:} requisito essenziale su cui si fonda tutto il progetto. Le condizioni dovranno essere recuperate in \textbf{realtime} e dovranno essere ottenute attraverso l'inferenza partendo da \textbf{valori raw} prodotte dai sensori installati in casa. Per ottenere il valore dei sensori ci si interfaccerà con \texttt{Home Assitant}, un \textbf{servizio di domotica open} al quale sono agganciati i sensori e che fornisce \texttt{Api REST} per il recupero di informazioni.
  \item \textbf{Condizioni meteo:} simile al requisito precedente ma in questo caso le informazioni andranno recuperate da un \textbf{servizio esterno}. Nel nostro caso si è scelto di utilizzare le Api di \texttt{OpenWeather}, che forniscono \textbf{gratuitamente} un ottimo servizio meteo.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.95\textwidth]{plantuml/rendered/useCaseDiagrams/TDOB-UseCase.pdf}
  \caption{Use case diagram relativo all'applicativo prodotto.}
  \label{fig:use_case}
\end{figure}

\section{Progettazione}
Dal punto di vista progettuale l'applicazione è strutturata seguendo le linee guida fornite dalla \textbf{libreria telegram}. Come vediamo nella figura ~\ref{fig:class}, classe \texttt{TelegramBot} rappresenta il \textbf{centro nevralgico} dell'applicativo.

\subsection{Agenti}
Al fine di sviluppare i due servizi di \textbf{recupero informazioni} in realtime si è reso necessario adottare un \textbf{approccio asincrono}; in particolare si è adottato il concetto di \texttt{Agent} ossia un'\textbf{entità autonoma} dedicata allo svolgimento di un compito particolare. Sono stati sviluppati due agenti entrambi di tipo \texttt{UpdateService}:
\begin{itemize}
  \item \texttt{UpdateService:} è la classe astratta che modella un \textbf{agente di recupero informazioni}. Esso possiede un insieme limitato di campi:
  \begin{itemize}
    \item \textit{owlManager:} è il manager dell'ontologia collegato.
    \item \textit{updateFrequency:} la frequenza di polling del servizio.
    \item \textit{endpoint:} l'\texttt{URL} del servizio utilizzato.
    \item \textit{token:} il token segreto utilizzato.
    \item \textit{homeName:} il nome della casa intelligente.
  \end{itemize}
  \item \texttt{WeatherUpdateService:} rappresenta il servizio di recupero informazioni dalla piattaforma \texttt{OpenWeather}. Esso richiede solamente la definizione del \textbf{nome della città} in cui si vive e automaticamente andrà a richiedere le informazioni necessarie.
  \item \texttt{HomeUpdateService:} anche questo servizio è molto semplice e definisce due ulteriori campi indicanti il path \texttt{REST} dei sensori necessari.
\end{itemize}

\subsection{Owl Manager}
La gestione dell'ontologia avviene attraverso il modulo \texttt{OwlManager}. Questo modulo essenzialmente si occupa delle seguenti \textbf{operazioni sull'ontologia}:
\begin{itemize}
  \item \textbf{Caricamento:} l'ontologia può essere caricata sia tramite \texttt{URL} che tramite local \texttt{URI}. Una volta caricata essa viene assimilata come una \textbf{struttura dati} a tutti gli effetti e può essere acceduta utilizzando la classica \textbf{notazione object }oriented.
  \item \textbf{Inferenza:} sull'ontologia è possibile fare inferenza utilizzando i due principali \textbf{reasoners} tuttora disponibili: \texttt{Pellet} e \texttt{Hermit}. È possibile inoltre specificare se effettuare inferenza solo di \textbf{classi} o anche di \textbf{proprietà}.
  \item \textbf{Salvataggio:} una volta svolta la fase di inferenza è possibile ovviamente salvare l'ontologia risultate. Nel nostro caso verrà \textbf{sovrascritto} più volte lo stesso file aggiornandolo.
  \item \textbf{Interrogazioni:} infine sarà possibile effettuare \textbf{interrogazioni} \texttt{Sparql} semplicemente interfacciandosi all'ontologia stessa attraverso \textbf{apposite Api}.
\end{itemize}

\subsection{Conversations}
A livello di Bot Telegram le conversazioni sono gestite attraverso una \textbf{macchina a stati finiti} \texttt{FSM} che permette di trattare ogni \textbf{conversazione} in maniera \textbf{isolata}. Sono stati definiti un numero di stati fondamentali:
\begin{itemize}
  \item \texttt{Not Logged:} quando l'utente si connette non è loggato e può solamente inviare un \textbf{messagio di login}. A questo punto gli verranno richieste le credenziali in un \textbf{formato predefinito} (\texttt{user:password}).
  \item \texttt{Logged:} se le credenziali sono corrette l'utente passerà allo stato di logged e qui verranno abilitati i comandi \texttt{HomeCondition} \texttt{WeatherCondition} e ovviamente \texttt{Logout}.
  \item \texttt{Request:} se viene scelto uno stato di tipo \texttt{*Condition} allora l'utente potrà scegliere una\textbf{ data} e il sistema restituirà l'\textbf{insieme di condizioni inferite} in quella giornata.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.95\textwidth]{drawio/DomoticOntology.pdf}
  \caption{Diagramma di classi relativo all'applicativo prodotto.}
  \label{fig:class}
\end{figure}

\section{Implementazione}
La fase di \textbf{implementazione dell'architettura} si è svolta in modo \textbf{lineare}: utilizzando un ottimo approccio di project management infatti è stato possibile prevedere alcuni \textbf{errori architetturali} correggendoli anticipatamente. Alcune problematiche sono state riscontrate nella fase di interfacciamento con la \textbf{libreria} dedicata alla \textbf{gestione dell'ontologia}.

\subsection{La libreria Owlready2}
Relativamente alla gestione dell'ontologia si è deciso di adottare un framework owl innovativo, \texttt{owlready2}. \texttt{Owlready2} è una \textbf{libreria python} basata su \texttt{Jena} dedicata alla \textbf{gestione ad alto livello} di ontologie owl. 

La libreria è veramente ben fatta e presenta un'integrazione massiccia definita \texttt{owl-python}. Questa integrazione permette di trattare l'ontologia in modo molto semplice assimilandola ad una \textbf{grande struttura dati} in un linguaggio ad oggetti. Questo permette di accedere a \textbf{classi} e \textbf{proprietà} in modo intuitivo utilizzando semplicemente il \texttt{.}. Ogni dato in \texttt{owlready2} è modificabile e ogni modifica ha \textbf{effetto immediato} all'interno dell'ontologia.

\subsubsection{Reasoning con Owlready2}
Il reasoning è estremamente semplice, è richiesto solamente definire un ambiente python con \texttt{with} in modo da definire la \textbf{destinazione} della nuova ontologia inferita. Inoltre è possibile definire un insieme di personalizzazioni riguardo al \textbf{cosa inferire}: \texttt{infer\_property\_values=True}.

\lstinputlisting[caption={Gestione ontologie con Owlready2}, language=python, label=lst:listato7]{code/owlready.py}

\subsubsection{Query con Owlready2}
\texttt{Owlready2} non supporta nativamente le query \texttt{Sparql} ma le garantisce fornendo un'astrazione di \textbf{grafo relazionale}. Il grafo prodotto da \texttt{Owlready2}, infatti, è compatibile con un'altra libreria estremamente famosa, \texttt{Rdflib}, una libreria dedicata al \textbf{processing di grafi} \texttt{RDF}. \texttt{Rdflib} fornisce nativamente un'ottima interfaccia per \texttt{Sparql} che garantisce \textbf{query dirette} e \textbf{query preparate}.

Come vediamo dall'esempio in \ref{lst:listato8} al fine di abilitare le query \texttt{Sparql} basta richiamare il metodo \texttt{as\_rdflib\_graph} presente nella proprietà \texttt{world}. Così facendo \texttt{Owlready2} andrà a creare il grafo compatibile con \texttt{Rdflib} e quindi con \texttt{Sparql}.

Fatto ciò è possibile creare una query; ricordiamo che la query \textbf{non viene modificata} dall'utente ma viene preparata da \texttt{Rdflib} con \texttt{prepareQuery}. Una volta che la query è preparata basterà solamente \textbf{iniettare} dall'esterno i valori richiesti, nel nostro caso l'\texttt{username}. Completato questo passaggio sarà possibile eseguire l'interrogazione e i \texttt{risultati} ottenuti saranno disponibili in una strutta dati di tipo \texttt{list}.

\lstinputlisting[caption={Rdflib creazione query}, language=python, label=lst:listato8]{code/rdflib_query.py}

\section{Screenshot TDOB}
Una volta fatto partire il bot verrà mostrata una \textbf{schermata di benvenuto}.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.80\textwidth]{bot_welcome}
  \caption{Welcome message inviato dal bot.}
  \label{fig:bot_welcome}
\end{figure}

Inviando il comando \texttt{\textbackslash login} sarà possibile autenticarsi al bot con \textbf{credenziali} nella forma \texttt{user:password}.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.80\textwidth]{bot_credentials}
  \caption{Credenziali richieste all'utente.}
  \label{fig:bot_credentials}
\end{figure}

Una volta autenticati verranno inviate all'utente le \textbf{informazioni generali} riguardo la condizione casalinga. Inoltre verranno inferiti automaticamente i \textbf{sensori attualmente collegati}. Nello stesso istante verranno messi in esecuzione i due \texttt{agenti} dedicati al recupero delle \textbf{informazioni meteo} e \textbf{sensoristiche}.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{bot_info}
  \caption{Informazioni generali inferite.}
  \label{fig:bot_info}
\end{figure}

In qualsiasi momento si può richiedere la \textbf{condizione meteo} o \textbf{casalinga}.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.80\textwidth]{bot_menu}
  \caption{Menu mostrato all'utente.}
  \label{fig:bot_menu}
\end{figure}

La richiesta del report si basa sull'inserimento di un \textbf{offset temporale in giorni}. Inserendo \texttt{0}, per esempio, verranno recuperate le condizioni relative a \texttt{0} giorni passati ossia il giorno stesso.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{bot_keyboard}
  \caption{Inserimento offset temporale.}
  \label{fig:bot_keyboard}
\end{figure}

La risposta contiene la \textbf{data} del report e la \textbf{condizione} meteo correlata. Nel caso in cui ci siano più condizioni, come in \ref{fig:bot_weather} esse verranno elencate di seguito una all'altra.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.60\textwidth]{bot_weather}
  \caption{Risposta con condizioni meteo inferite.}
  \label{fig:bot_weather}
\end{figure}

La risposta relativa alle \textbf{condizioni casalinghe} segue lo \textbf{stesso schema} visto per le condizioni meteo.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{bot_home}
  \caption{Risposta con condizioni ambientali inferite.}
  \label{fig:bot_home}
\end{figure}

