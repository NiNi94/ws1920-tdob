% -*- root: ../main.tex -*-
\chapter{Domotic Ontology}
In questo capitolo descriveremo i passi fondamentali che hanno portato alla definizione dell'ontologia \texttt{Domotic Ontology}. Inizialmente ci concentreremo sulla descrizione dell'ontologia sorgente \texttt{Weather Ontology} studiando in dettaglio l'\textbf{origine} della stessa, l'\textbf{organizzazione promotrice} e le \textbf{motivazioni} che hanno guidato il suo sviluppo. Successivamente ci addentreremo nell'analisi del \textbf{processo di espansione} dell'ontologia, esponendo le \textbf{scelte} fatte, i \textbf{problemi} riscontrati e le \textbf{soluzioni} adottate. Infine descriveremo l'insieme di \textbf{query} \texttt{Sparql} che verranno poi utilizzate all'interno dell'applicativo sviluppato.

\section{Weather Ontology}
Come introdotto nello scorso capitolo, l'ontologia \textbf{Weather Ontology} è dedicata principalmente alla modellazione di \textbf{previsioni} e \textbf{condizioni metereologiche}. Prima di entrare nel merito dell'ontologia stessa è necessario introdurre alcune informazioni di contorno riguardo le origini e le motivazioni che hanno portato allo sviluppo dell'ontologia stessa.  

\subsection{Il progetto ThinkHome}
L'ontologia \textbf{Weather Ontology} è stata sviluppata dal gruppo di ricerca \textbf{Automation System Group} dell'istituto di computer engineering a Vienna. In particolare il gruppo fa parte di un ateneo molto rinomato, il \texttt{TU-Vien} di Vienna. Il \texttt{TU-wien} è una delle università più importanti di Vienna. L'università, conosciuta anche come \textbf{Politecnico di Vienna}, ha ricevuto ampi riconoscimenti internazionali e nazionali nell'insegnamento e nella ricerca ed è un partner molto \textbf{stimato delle imprese} orientate all'\textbf{innovazione}. Attualmente conta circa 28.100 studenti e le tematiche di ricerca dell'università si concentrano su \textbf{ingegneria}, \textbf{informatica} e \textbf{scienze naturali}. 

Uno degli ambiti di ricerca più floridi all'interno del gruppo di ricerca \textbf{Automation System Group} riguarda lo sviluppo di ontologie volte alla modellazione di concetti elaborati dal gruppo. Oltre all'ontologia in questione, infatti, il gruppo ha sviluppato una vasta gamma di ontologie tutte dedicate al mondo \texttt{IOT}, \textbf{industria 4.0} e \textbf{Home Automation}. L'integrazione di un numero estremamente elevato di tecnologie ha portato alla realizzazione di un progetto molto avanzato, chiamato \textbf{ThinkHome} (fig: ~\ref{fig:thinkhome}).

\begin{figure}[htbp]
   \centering
   \includegraphics[width=0.95\textwidth]{thinkhome}
   \caption{Il progetto ThinkHome}
   \label{fig:thinkhome}
 \end{figure}

 Come si può vedere il progetto \textbf{ThinkHome} utilizza le ontologie in modo estremamente intelligente, trattandole non come un semplice database ma come \textbf{knowledge base} dinamica capace di \textbf{inferire nuovi dati}.

\subsubsection{Ontologie modulari}
Le ontologie utilizzate all'interno del progetto \textbf{ThinkHome} sono gestite in modo modulare, ossia ogni singola ontologia gestisce un preciso contesto differente da tutti gli altri. Questo disaccoppiamento permette al gruppo di lavorare in modo sinergico su più fronti al fine di creare parallelamente una \textbf{rete di conoscenza} estremamente \textbf{completa} evitando tutte le \textbf{problematiche} correlate con le \textbf{ontologie più complesse}. Fra queste ontologie \textbf{Weather Ontology} rappresenta un piccolo modulo dedicato alla modellazione di fenomeni di tipo metereologico.

\subsection{Analisi dell'Ontologia - Classi}
\textbf{Weather Ontology} è un'ontologia estremamente semplice eppure mette in gioco un insieme di pattern davvero interessanti. Essa è suddivisa, come vediamo in figura \ref{fig:wo_classes}, in cinque classi principali così ordinate:

\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.70\textwidth]{wo_classes}
  \caption{Elenco delle classi presenti all'interno di Weather Ontology}
  \label{fig:wo_classes}
\end{figure}

\begin{itemize}
  \item \textbf{Weather Report:} rappresenta un \textbf{report} dedicato a una previsione metereologica. Esso fa riferimento a una \textbf{zona geografica} e a un certo \textbf{istante temporale} modellato tramite la libreria esterna \textbf{OWL-Time}.
  \item \textbf{Weather Report Source:} rappresenta una sorgente di informazioni come \textbf{sensori} e \textbf{sorgenti web}.
  \item \textbf{Weather State:} definisce uno stato misurato in un certo \textbf{istante temporale}, ovviamente ad esso saranno associate una serie di \textbf{misurazioni}.
  \item \textbf{Weather Phenomenon:} costituisce un \textbf{fenomeno metereologico}.
  \item \textbf{Weather Condition:} rappresenta una \textbf{condizione meteo}.
\end{itemize}

Qui di seguito andiamo ad analizzare ogni singola entità qua sopra introdotta:

\subsubsection{Weather Report}
Un \textbf{Weather Report} costituisce un qualsiasi report, questo significa che al suo interno è possibile trovare sia il significato di \textbf{condizione} che il significato di \textbf{previsione} meteo. L'ontologia per modellare il concetto di previsione attua una scelta molto particolare: essa elenca infatti \textbf{tutte le ore} per cui è possibile fare una previsione meteo mentre per i giorni riunisce il tutto sotto tre concetti fondamentali: \texttt{ShortRange}, \texttt{MidRange} e \texttt{LongRange}. 
\begin{figure}[H]
  \centering
  \includegraphics[width=0.50\textwidth]{wo_report}
  \caption{Focus su Weather Report.}
  \label{fig:wo_report}
\end{figure}

\subsubsection{Weather Report Source}
In questo caso la modellazione è molto semplice: essa suddivide le sorgenti di report in due categorie: \textbf{Sensori} e \textbf{Servizi Esterni}. Questa lassità di modellazione permette così di avere grande libertà nel momento in cui si voglia aggiungere altre sorgenti.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{wo_source}
  \caption{Focus su Weather Report Source.}
  \label{fig:wo_source}
\end{figure}

\subsubsection{Weather State}
Uno stato è molto simile al concetto di \textbf{Report} ma deve essere lasciato separato al fine di implementare un \textbf{pattern di progettazione} molto importante nel campo delle ontologie. Grazie a questo pattern uno stato può \textbf{cambiare classe} in base alle condizioni del fenomeno sottostante.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{wo_state}
  \caption{Focus su Weather State.}
  \label{fig:wo_state}
\end{figure}

\subsubsection{Weather Phenomenon}
Questa entità è estremamente importante e modella i \textbf{dati sorgenti} dell'ontologia. Un fenomeno meteo può avere tantissime forme e l'ontologia ne presenta un numero considerevole. Anche in questo caso è stato sfruttato il pattern nominato precedentemente al fine di effettuare un \textbf{casting di inferenza su fenomeni particolari}, per esempio \textbf{Dry/Moist/Optimum Humidity}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{wo_phenomenon}
  \caption{Focus su Weather Phenomenon.}
  \label{fig:wo_phenomenon}
\end{figure}

\subsubsection{Weather Condition}
Infine le condizioni meteo utilizzano un altro pattern di progettazione ossia l'\textbf{utilizzo di istanze come forma di enumerazione di stato}. Come vediamo infatti esistono un numero esatto di condizioni che possono essere assunte da uno stato metereologico e queste sono correlate ai fenomeni sottostanti e agli stati inferiti.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.20\textwidth]{wo_condition}
  \caption{Focus su Weather Condition.}
  \label{fig:wo_condition}
\end{figure}

\subsection{Analisi dell'Ontologia - Proprietà}
Relativamente alle proprietà esse sono estremamente \textbf{lineari} e \textbf{autoesplicative}; ogni singola proprietà infatti si collega molto bene ad ogni classe e ne definisce un collegamento univoco con un'altra classe (fig: \ref{fig:wo_object_properties}).
\begin{figure}[htbp]
  \centering
  \includegraphics[width=0.60\textwidth]{wo_object_properties}
  \caption{Elenco delle classi presenti all'interno di Weather Ontology}
  \label{fig:wo_object_properties}
\end{figure}

\section{Espansione ontologia - Domotic Ontology}
Durante l'espansione dell'ontologia ci si è soffermati sopratutto sull'introduzione del \textbf{concetto di casa intelligente}, per questo motivo sono state introdotte una serie di classi dedicate a questo contesto: 
\begin{figure}[H]
  \centering
  \includegraphics[width=0.40\textwidth]{do_classes}
  \caption{Elenco delle classi presenti all'interno di Domotic Ontology}
  \label{fig:do_classes}
\end{figure}

\begin{itemize}
  \item \textbf{Home:} rappresenta una \textbf{casa intelligente} nella quale vivono \textbf{Persone} e nella quale sono installati \textbf{Sensori}.
  \item \textbf{Person:} è un \textbf{individuo} che può abitare una smart home.
  \item \textbf{Location:} è la location di una casa intelligente a cui è associata una \textbf{città}, una \textbf{latitudine} e una \textbf{longitudine}.
  \item \textbf{Home Report:} ad ogni casa intelligente sono associati uno o più \textbf{report sulle condizioni ambientali} delle stesse. Il report è individuato in base all'\textbf{orario di creazione}.
  \item \textbf{Home State:} è uno stato descritto da un singolo \textbf{Report}. Ogni stato elenca una serie di misurazioni.
  \item \textbf{Home Measurement:} è una misurazione legata a una \textbf{grandezza fisica}.
  \item \textbf{Home Condition:} è una condizione ambientale presente all'interno della casa.
  \item \textbf{Sensor:} è la rappresentazione di un sensore fisico.
\end{itemize}

\subsection{Pattern utilizzati}
Nello sviluppo dell'ontologia sono stati utilizzati alcuni pattern di progettazione molto interessanti. Grazie ad essi è possibile semplificare di molto l'ontologia prodotta spostando la complessità sul fronte dell'\textbf{inferenza di informazioni}.

\subsubsection{Dynamic casting}
Il primo pattern è stato denominato \texttt{Dynamic Casting} e consiste nel definire una gerarchia di classi in cui la classe \textbf{padre} può assumere le \textbf{sembianze} di una o più classi figlie. Questo è per esempio il caso di \texttt{HomeTemperature} (fig:~\ref{fig:do_home_temp}) per la quale nella condizione in cui la proprietà \texttt{hasValue} sia collegata a un valore <\SI{16}{\celsius} questa si trasforma in \texttt{ColdHomeTemperature}. 

\begin{figure}[H]
\begin{subfigure}{.49\textwidth}
  \centering
  % include first image
  \includegraphics[width=.8\linewidth]{do_home_temp}  
  \caption{Esempio: \texttt{HomeTemperture}}
  \label{fig:do_home_temp}
\end{subfigure}
\begin{subfigure}{.49\textwidth}
  \centering
  % include second image
  \includegraphics[width=.8\linewidth]{do_cold}  
  \caption{Esempio: \texttt{ColdHomeTemperture}}
  \label{fig:do_cold}
\end{subfigure}
\caption{Esempio di Dynamic casting.}
\label{fig:do_dynamic_casting}
\end{figure}

Il pattern è stato usato in maniera estensiva per un numero molto grande di entità. L'aspetto più interessante del pattern consiste proprio nel \textbf{generare automaticamente}, \textbf{tramite inferenza}, una catena di risultati a partire da un insieme di valori di tipo \textbf{raw}.

\subsubsection{Instances}
L'uso di istanze infine è stato molto utile per creare un insieme di condizioni ambientali e metereologiche \textbf{standard} da affiancare al \textbf{Dynamic Casting}. Così facendo è possibile elencare solo una gamma limitata di condizioni possibili \textbf{semplificando} di molto la \textbf{fase di inferenza}.

\section{Query SPARQL}
Uno dei grandi vantaggi che si ottengono utilizzando un'ontologia risiede nella facilità di effettuare \textbf{interrogazioni} (Query). Al contrario di un database, infatti, un'ontologia predilige una \textbf{base di conoscenza aperta} che può essere interrogata in modo dinamico dall'utente. Una knowledge base definisce aperta quando le informazioni che è possibile ottenere da essa non esistono concretamente all'interno dell'ontologia; esse possono essere prodotte da processi di \textbf{ragionamento} (inferenza) capaci, appunto, appunto di \textbf{generare nuova conoscenza}. Di seguito verranno elencate le principali interrogazioni utilizzate nell'applicativo sviluppato.

\subsection{Credenziali}
Una delle prime query sviluppate è estremamente semplice e permette di \textbf{validare le credenziali} di un utente.
\lstinputlisting[caption={Credentials}, label=lst:listato1]{code/credentials.sql}

\subsection{Informazioni generali}
Successivamente sono state sviluppate una serie di \textbf{query modulari} dedicate al recupero dall'ontologia di varie tipologie di \textbf{informazioni}.
\begin{itemize}
  \item \textit{Home name:} per recuperare il nome della casa in cui è presente l'utente basta semplicemente recuperare il nome dell'utente stesso e successivamente ottenere la casa in cui \textbf{esso vive} (\texttt{inhabitedBy}) e quindi il nome.
  \lstinputlisting[caption={Home name}, label=lst:listato2]{code/home_name.sql}

  \item \textit{City name:} l'estrazione del nome della città segue lo stesso percorso per quanto riguarda la casa ma stavolta si richiede un'indirezione ulteriore relativa alla \textbf{location} (\texttt{hasLocation}) in cui si trova la casa dell'utente.
  \lstinputlisting[caption={City name}, label=lst:listato3]{code/city_name.sql}

  \item \textit{Sensors name:} infine per recuperare i nomi dei sensori installati basta recuperare sempre la casa in cui vive l'utente andando però a richiedere i sensori \textbf{installati} (\texttt{hasInstalledSensor}).
  \lstinputlisting[caption={Sensors name}, label=lst:listato4]{code/sensor_name.sql}
\end{itemize}

\subsection{Condizioni ambientali}
Le query riferite alle condizioni ambientali sono quelle più importanti relativamente all'applicativo sviluppato. Sono state prodotte due tipologie di interrogazioni: una dedicata all'aspetto \textbf{metereologico} e l'altra dedicata all'aspetto \textbf{ambientale casalingo}.

\subsubsection{Weather condition}
Per quanto riguarda le condizioni metereologiche la query proposta si presenta di carattere abbastanza complesso ma rimane tutto sommato comprensibile.
\lstinputlisting[caption={Weather condition}, label=lst:listato5]{code/weather_condition.sql}
In questo caso si recupera sempre il concetto di casa ma questa volta si richiede tutti i \textbf{report associati} ad essa (\texttt{refersToWeatherReport}). Una volta ottenuti i report, si richiede che la \textbf{data} degli stessi si trovi in un range limitato: questo è possibile utilizzando il termine \texttt{Filter} di \texttt{Sparql}. Una volta ottenuti tutti i report prodotti nel range di date definito, andiamo a richiede lo \textbf{stato inferito} dal sistema e collegato al suddetto report (\texttt{refersToWeatherState}). Infine, ottenuto lo stato possiamo richiedere quale fosse la \textbf{condizione meteo} inferita dal sistema in quel dato istante.

\subsection{Home condition}
Relativamente all'aspetto \textbf{smart home}, le cose non cambiano molto: la query è praticamente la stessa ma è traslata al fine di essere \textbf{compatibile} con i concetti precedentemente introdotti.

\lstinputlisting[caption={Home condition}, label=lst:listato6]{code/home_condition.sql}
Lo sviluppo della query ricalca quello mostrato in precedenza e l'unica deviazione avviene per quanto riguarda l'estrazione della tipologia di report richiesta, tramite appunto \texttt{hasHomeReport}. 







